#!/usr/bin/env python3
#
# $ python -m pip install PyCryptodome

from Crypto.Cipher import AES
import codecs

### AES-CBC Decrypt from hex string
key = codecs.decode('9fc55cb19413c5fc3e0e6e790a360390', 'hex_codec')
iv = codecs.decode('f5fc5ae8eefd760023e7d94e38ad0290', 'hex_codec')
enc = codecs.decode('e7784e590218b8899c2af32aabd29e7fa4cf81391198dd0df74debefae853123', 'hex_codec')

cipher = AES.new(key, AES.MODE_CBC, iv)
print (cipher.decrypt(enc))
